# bieg

Running statistics (mean, max, min, median, etc.) for R.
Work in progress.

Aims:

- State-of-the-art algorithms for speed, written in C
- Explicit points-before points-after window declaration
- Boundary conditions instead of clipping of the output
- Parallelism with OpenMP, where it does make sense (on rows/columns/list elements)
- window-sapply/window-lapply support



