#include <R.h>
#include <Rdefines.h>
#include <Rinternals.h>
#include <R_ext/Utils.h>
#include <R_ext/Rdynload.h>
#include <R_ext/Visibility.h> 

#include "deq.h"
#include "runningMax.h"
#include "runningMin.h"
#include "runningSum.h"

#define CALLDEF(name, n)  {#name, (DL_FUNC) &name, n}
static const R_CallMethodDef R_CallDef[]={
 CALLDEF(C_runningMax,3),
 CALLDEF(C_runningMin,3),
 CALLDEF(C_runningSum,3),
 {NULL,NULL,0}
};

void attribute_visible R_init_bieg(DllInfo *dll){
 R_registerRoutines(dll,NULL,R_CallDef,NULL,NULL);
 R_useDynamicSymbols(dll,FALSE);
 R_forceSymbols(dll,TRUE);
}
  
