struct deq{
 size_t cap;
 int *left;
 int *right;
 int cont[1];
};

struct deq* makeDeq(size_t cap){
 struct deq* ans=(struct deq*)R_alloc(sizeof(struct deq)+(sizeof(int)*(cap-1)),1);
 if(!ans) return(ans);
 ans->cap=cap;
 ans->left=ans->right=NULL;
 return(ans);
}

void pushLeftDeq(struct deq *Q,int x){
 if(!Q->left){
  (Q->left=Q->right=Q->cont)[0]=x;
  return;
 }

 size_t cap=Q->cap;
 Q->left=Q->cont+((cap+(Q->left-Q->cont)-1)%cap);
 if(Q->right==Q->left) error("Deque overflow");
 Q->left[0]=x; 
}

void pushRightDeq(struct deq *Q,int x){
 if(!Q->right){
  (Q->left=Q->right=Q->cont)[0]=x;
  return;
 }

 size_t cap=Q->cap;
 Q->right=Q->cont+(((Q->right-Q->cont)+1)%cap);
 if(Q->right==Q->left) error("Deque overflow");
 Q->right[0]=x;
}

int popLeftDeq(struct deq *Q){
 if(!Q->left) error("Pop from an empty deque");
 int ans=*(Q->left);
 size_t cap=Q->cap;
 if(Q->left==Q->right){
  //Empty again!
  Q->left=Q->right=NULL;
  return(ans);
 }
 Q->left=Q->cont+(((Q->left-Q->cont)+1)%cap);
 return(ans);
}

int popRightDeq(struct deq *Q){
 if(!Q->right) error("Pop from an empty deque");
 int ans=*(Q->right);
 size_t cap=Q->cap;
 if(Q->left==Q->right){
  Q->left=Q->right=NULL;
  return(ans);
 }
 Q->right=Q->cont+((cap+(Q->right-Q->cont)-1)%cap);
 return(ans);
}

