void minFilter(double *inpt,double *outp,int N,int b,int a){
 int k=1+a+b;
 struct deq *J=makeDeq(k);
 //Put afters
 for(int e=0;e<a+1;e++){
  for(;
   (J->right) && (inpt[e]<=inpt[*(J->right)]);
   popRightDeq(J)
  );
  pushRightDeq(J,e);
 }
 for(int e=0;e<N;e++){
  outp[e]=inpt[*J->left];
  //Update J:
  //Remove left elements older than b
  for(;
   (J->left) && (*(J->left)<=e-b);
   popLeftDeq(J)
  );
  //Remove elements smaller than cur+a 
  if(e+a+1<N){
   for(;
    (J->right) && (inpt[*(J->right)]>=inpt[e+a+1]);
    popRightDeq(J)
   );
   //Add current
   pushRightDeq(J,e+a+1);
  }
 }
}

SEXP C_runningMin(SEXP X,SEXP B,SEXP A){
 int N=length(X);
 int a=INTEGER(A)[0];
 int b=INTEGER(B)[0];

 SEXP Ans=PROTECT(allocVector(REALSXP,N));
 double *x=REAL(X),*ans=REAL(Ans);
 minFilter(x,ans,N,b,a);
 UNPROTECT(1);
 return(Ans);
}

