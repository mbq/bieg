void sumFilter(double *inpt,double *outp,int N,int b,int a,int nrm){
 //J contains from -b to a-1
 int Je=0,k=a+b+1;
 double *J=(double*)R_alloc(sizeof(double),k),cumsum=0.;
 //J contains values from 
 //Put befores
 for(int e=-b-1;e<N+a;e++){
  if(e>=0 && e<N) cumsum+=inpt[e];
  if(e>=a){
   if(!nrm) 
    outp[e-a]=cumsum-J[Je];
   else
    outp[e-a]=(cumsum-J[Je])/((double)k); //TODO: Fix this
  }
  J[Je]=cumsum; //Store cumsum in head
  Je=(Je+1)%k;
 }
}

SEXP C_runningSum(SEXP X,SEXP B,SEXP A){
 int N=length(X);
 int a=INTEGER(A)[0];
 int b=INTEGER(B)[0];

 SEXP Ans=PROTECT(allocVector(REALSXP,N));
 double *x=REAL(X),*ans=REAL(Ans);
 sumFilter(x,ans,N,b,a,3);
 UNPROTECT(1);
 return(Ans);
}

